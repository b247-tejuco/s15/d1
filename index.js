// [SECTION] Syntax, Statements and Comments

//Single Line Comments
/*In Between / Multi-line  Comments*/

console.log('Voren'); //JS Statements usually ends with semicolon(;), meaning the line is done

// [SECTION] Variables

// It is used to contain data
// container or storage

// Declaring Variables
// Declaring Variables -tells our devices that a variable name is created and is ready to store data.

let name;
console.log(name); // undefined
// Declaring a variable without giving a value will automatically assign the value of 'undefined'

let name1= 'Elver';
console.log(name1);

// Declaring and Initializing Variables
// Initializing Variables - The instance when a variable is given it's initial starting value

/*Reassigning Variable Values **JS reads code from Top to Bottom** 
Would print latest reassignment hence dog */
let animal;
console.log(animal); // undefined

animal = "cat";
console.log(animal);

animal ="dog";
console.log(animal);

const interest = 3.456;
console.log(interest);

/*interest = 4.0;
console.log(interest);*/ /* Error because Constant cant be changed ex. pi value 3.14asdasd */

const color = 'blue';
console.log(color);


// [SECTION] Data Types

// Strings
// Series of characters that create a word, phrase, a sentence
// Strings in JS can be written using either a single ('') or double (" ")quote

let country= 'Philippines';
let province= "Metro Manila";
let myAge= 24;	/*is not a string type*/
let myEdad= '24'; /*is a string*/

// Multiple Variable Declarations
console.log(province, country);
console.log(myAge, myEdad);

// Concatenating Strings
// Multiple string values can be combined to create a single string using the + symbol

let fullAddress=province+", "+ country;
console.log(fullAddress);

let greeting='I live in the '+ country;
console.log(greeting);

// The escape character (\)
// the '\n' refers to creating a new line in between text sorta like br tag from html

let mainAddress='Metro Manila\n\nPhilippines'
console.log(mainAddress);

let message="John's employees went home early";
console.log(message);
message= 'John\'s employees went home early';
console.log(message);

// Numbers
// Integers/Whole Numbers

let headcount= 26;
console.log(headcount);
let headcount0= 1000000;
console.log(headcount0);
let headcount1= 1_000_000;
console.log(headcount1);

// decimal number/fractions

let grade= 98.9;
console.log(grade);

// exponential notation

let planetDistance= 2e10;
console.log(planetDistance);

//combining text and strings
console.log("John's grade last quarter is "+grade);

//Boolean
/*Boolean values are normally used to store values
relating to the state of certain things ~true or false */

let isMarried= false;
let inGoodConduct= true;

console.log("isMarried: "+ isMarried);
console.log("inGoodConduct: "+ inGoodConduct);

/*Arrays*/
/*Arrays are a special kind of data type
that's used to store multiple values*/
//array = [0, 1, 2, 3] index value

/*Syntax
 let/const arrayName = [alpha, bravo, charlie];*/

 let grades= [98.7, 92.1, 90.0, 88.4];
 console.log(grades);

 //different data types

 let details= ["John", "Smith", 32, false];
 console.log(details);

//Objects
/*Objects are another special kind of data type that's
used to mimic real world objects/items*/

//Syntax

	/*let/const/objectName= {
		propertyA: value,
		propertyB: value

	}*/

let person= {
	fullName: 'Michael Jordan',
	age: 35,
	isMarried: true,
	jerseyNumber: [23, 45, 35],
	city: ["Chicago", "Wizards"],
	address: {
		houseNumber: '23',
		city: 'Chicago'
	}
}

console.log(person);

const myGrades= {
	firstYear: 1.67,
	secondYear: 1.05,
	thirdYear: 1.00,
	fourthYear: 1.5
}

console.log(myGrades);

/* type of operator is used to determined the type of data*/

console.log(typeof myGrades);
console.log(typeof myEdad);
console.log(typeof grades);
console.log(typeof isMarried);

const anime =['one piece', 'one punch man', 'attack on titan', 'naruto'];

anime[0]= ["bluelock"];

console.log(anime)





/*Constant Objects and Arrays
	the keyword const is a little misleading.

	It does not define a constant value. Rather, it defines a constant
	reference to a value.

	because of this, we can NOT:

	Reassign a constant value
	Reassign a constant array
	Reassign a constant object

	But you CAN
	
	Change the elements of constant array
	change the properties of constant object
	*/

	//EXAMPLE
	//const anime= 'kuroko';
	//console.log(anime); SYNTAX EERROOR*


	/*Null*/

	/*It is used to intentionally express the absence
	of a value in a varriable declaration/initialization*/

	let spouse= null;
	console.log(spouse);

	/*Using null compared to a 0 value and an empty
	string is much better for reading purposes*/

	let myNumber= 0;
	let myString= '';
	console.log(myNumber);
	console.log(myString);

	//undefined = no value yet

	let fullName;
	console.log(fullName);

	/*Undefined vs Null*/

	let varA= null;
	console.log(varA);

	let varB;
	console.log(varB);
	/*null means that a variable was created and 
	was assigned a value that does not hold any amt/value*/

	/*Undefined is when the value of a variable is still unknown*/


